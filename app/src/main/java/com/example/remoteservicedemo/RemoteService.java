package com.example.remoteservicedemo;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.example.myapplication.IDownloadCallback;
import com.example.myapplication.IRemoteInterface;

public class RemoteService extends Service {
    IDownloadCallback mDownloadCallback;

    public RemoteService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mStub;
    }

    private IRemoteInterface.Stub mStub = new IRemoteInterface.Stub() {
        @Override
        public void registerCallback(IDownloadCallback callback) throws RemoteException {
            mDownloadCallback = callback;
        }

        @Override
        public boolean downloadFile(String url) {
            Log.i("RemoteService", "Start download file from : " + url);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i <= 100; i += 100 / 5) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (mDownloadCallback != null) {
                            try {
                                mDownloadCallback.updatePercentage(i);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }).start();
            return true;
        }
    };

}